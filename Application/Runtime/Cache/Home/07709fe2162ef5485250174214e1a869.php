<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>二维码管理系统</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp"/>
        <link rel="alternate icon" type="image/png" href="/QrcodeManager/Public/i/favicon.png">
        <link rel="stylesheet" href="/QrcodeManager/Public/css/amazeui.min.css"/>
        <link rel="stylesheet" href="/QrcodeManager/Public/css/admin.css">
        
        <!--[if lt IE 9]>
        <script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
        <script src="/QrcodeManager/Public/js/amazeui.ie8polyfill.min.js"></script>
        <![endif]-->

        <!--[if (gte IE 9)|!(IE)]><!-->
        <script src="/QrcodeManager/Public/js/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="/QrcodeManager/Public/js/amazeui.min.js"></script>

        <script type="text/javascript">
            function exit() {
                $('#exit-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        window.location = "/QrcodeManager/index.php/Home/Index/logout";
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        //alert('算求，不退出了');
                    }
                });
            }
        </script>
    </head>
    <body>
        <!--[if lte IE 9]>
        <p class="browsehappy">本网站不支持<strong>过时</strong>的浏览器。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
        <![endif]-->

        <header class="am-topbar admin-header">
            <div class="am-topbar-brand">
                <strong>澄城人武部</strong> <small>二维码体检管理系统</small>
            </div>

            <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

            <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

                <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
                    <li><a href="javascript:;"><span class="am-icon-cog"></span> 设置</a></li>
                    <li><a href="javascript:exit();"><span class="am-icon-power-off"></span> 退出</a></li>
                </ul>
            </div>
        </header>

        <div class="am-cf admin-main">
            <!-- sidebar start -->
            <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
                <div class="am-offcanvas-bar admin-offcanvas-bar">
                    <ul class="am-list admin-sidebar-list">
                        <li><a href="/QrcodeManager/index.php/Home/Index/main"><span class="am-icon-home"></span> 首页</a></li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#renyuan-nav'}"><span class="am-icon-user-secret"></span> 人员管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="renyuan-nav">
                                <li><a href="/QrcodeManager/index.php/Home/People/index"><span class="am-icon-user-plus"></span> 人员资料管理</a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/qrcode"><span class="am-icon-qrcode"></span> 二维码打印表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/look"><span class="am-icon-qrcode"></span> 扫码查看人员 </a></li>
                            </ul>
                        </li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#tijian-nav'}"><span class="am-icon-file"></span> 体检管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="tijian-nav">
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/index"><span class="am-icon-dedent"></span> 体检资料管理</a></li>
                                <li><a href="admin-help.html"><span class="am-icon-table"></span> 体检表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/main"><span class="am-icon-table"></span> 体检结果 </a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="am-panel am-panel-default admin-sidebar-panel">
                        <div class="am-panel-bd">
                            <p><span class="am-icon-bookmark"></span> 现在时间：</p>
                            <script type="text/javascript" src='/QrcodeManager/Public/js/clock.js'></script>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->

            <!-- content start -->
            
<div class="admin-content">
    <div class="am-cf am-padding">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">测试一些功能用</strong></div>
    </div>
    <div class="am-g">
        <div class="am-u-sm-12">
            <form class="am-form" id="qrform" onsubmit="test()">
                <div class="am-form-group">
                    <label for="qrcode">字符串加解密</label>
                    <input type="text" id="s" name="s">
                </div>
                <div class="am-form-group">
                    <label class="am-radio-inline">
                        <input type="radio"  value="E" name="m"> 加密
                    </label>
                    <label class="am-radio-inline">
                        <input type="radio" value="D" name="m"> 解密
                    </label>
                    <p><button type="submit" class="am-btn am-btn-default">提交</button></p>
                </div>
            </form>
        </div>
        <div class="am-u-sm-12">
            <form class="am-form" id="qrform2" onsubmit="test2()">
                <div class="am-form-group">
                    <label for="s2">测试加密、不加密条件下数据库访问</label>
                    <input type="text" id="s2" name="s2">
                </div>
                <div class="am-form-group">
                    <label class="am-radio-inline">
                        <input type="radio"  value="E" name="m"> 加密
                    </label>
                    <label class="am-radio-inline">
                        <input type="radio" value="D" name="m"> 不加密
                    </label>
                    <p><button type="submit" class="am-btn am-btn-default">提交</button></p>
                </div>
            </form>
        </div>
    </div>
    <hr>
    <h1>测试结果：<span id="result"></span></h1>
</div>


<script type="text/javascript">
    function test() {
        event.preventDefault();
        $.ajax({
            url: "/QrcodeManager/index.php/Home/Tijian/testED",
            dataType: 'JSON',
            type: "POST",
            cache: false,
            data: $("#qrform").serialize(),
            success: function (data) {
                $("#result").html(data.s);
            },
            error: function () {
                alert("失败");
            }
        });
    }
    function test2() {
        event.preventDefault();
        $.ajax({
            url: "/QrcodeManager/index.php/Home/Tijian/testDatabase",
            dataType: 'JSON',
            type: "POST",
            cache: false,
            data: $("#qrform2").serialize(),
            success: function (data) {
                $("#result").html(data.s);
            },
            error: function () {
                alert("失败");
            }
        });
    }
</script>

            <!-- content end -->
        </div>
        <a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
        <footer class="footer" style="text-align: center;">
            <p>© 2015 by 澄城县人民武装部.</p>
        </footer>
        <div class="am-modal am-modal-confirm" tabindex="-1" id="exit-confirm">
            <div class="am-modal-dialog">
                <div class="am-modal-hd">退出</div>
                <div class="am-modal-bd">
                    确定要退出系统吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-confirm>确定</span>
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                </div>
            </div>
        </div>
        
    </body>
</html>