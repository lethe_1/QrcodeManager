<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>澄城人武部二维码管理系统 | 登录</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <link rel="alternate icon" type="image/png" href="/QrcodeManager/Public/i/favicon.png">
    <link rel="stylesheet" href="/QrcodeManager/Public/css/amazeui.min.css"/>
    <style>
        .header {
            text-align: center;
        }

        .header h1 {
            font-size: 200%;
            color: #333;
            margin-top: 30px;
        }

        .header p {
            font-size: 14px;
        }
    </style>
</head>
<body>
<div class="header">
    <div class="am-g">
        <h1>二维码管理系统</h1>

        <p>Integrated Development Environment<br/>二维码应用，生成，管理</p>
    </div>
    <hr/>
</div>
<div class="am-g">
    <div class="am-u-lg-6 am-u-md-8 am-u-sm-centered">
        <h3>登录</h3>
        <hr>
        <br〉

        <form method="post" class="am-form" action="/QrcodeManager/index.php/Index/login">
            <label for="username">用户名:</label>
            <input type="text" name="username" id="username" value="">
            <br>
            <label for="password">密码:</label>
            <input type="password" name="password" id="password" value="">
            <br>
            <label for="remember-me">
                <input id="remember-me" type="checkbox">
                记住密码
            </label>
            <br/>

            <div class="am-cf">
                <input type="submit" id="submitBtn" value="登 录" class="am-btn am-btn-primary am-btn-sm am-fl">
                <!--<input type="submit" name="" value="忘记密码 ^_^? " class="am-btn am-btn-default am-btn-sm am-fr">-->
            </div>

        </form>
        <hr>
        <p style="text-align: center;">© 2015 Lethe.</p>
    </div>
</div>

<!--[if lt IE 9]>
<script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
<script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
<script src="/QrcodeManager/Public/js/amazeui.ie8polyfill.min.js"></script>
<![endif]-->

<!--[if (gte IE 9)|!(IE)]><!-->
<script src="/QrcodeManager/Public/js/jquery.min.js"></script>
<!--<![endif]-->
<script src="/QrcodeManager/Public/js/amazeui.min.js"></script>

<script type="text/javascript">
    $(function(){
        $("#submitBtn").click(function(event){
            event.preventDefault();
            var username=$("#username").val();
            var password=$("#password").val();

            $.ajax({
                url:"/QrcodeManager/index.php/Home/Index/login",
                dataType:"json",
                type:"POST",
                cache:false,
                data:"username="+username+"&password="+password,
                success:function(data){
                    if(data.s=='ok'){
                        window.location="/QrcodeManager/index.php/Home/Index/main";
                    }else{
                        alert("登陆失败："+data.s);
                    }
                },
                error:function(){
                    alert("登陆失败：服务器错误！");
                }
            });
        });
    });
</script>
</body>
</html>