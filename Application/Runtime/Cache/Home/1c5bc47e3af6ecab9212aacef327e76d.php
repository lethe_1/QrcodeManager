<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>二维码管理系统</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp"/>
        <link rel="alternate icon" type="image/png" href="/QrcodeManager/Public/i/favicon.png">
        <link rel="stylesheet" href="/QrcodeManager/Public/css/amazeui.min.css"/>
        <link rel="stylesheet" href="/QrcodeManager/Public/css/admin.css">
        
        <!--[if lt IE 9]>
        <script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
        <script src="/QrcodeManager/Public/js/amazeui.ie8polyfill.min.js"></script>
        <![endif]-->

        <!--[if (gte IE 9)|!(IE)]><!-->
        <script src="/QrcodeManager/Public/js/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="/QrcodeManager/Public/js/amazeui.min.js"></script>

        <script type="text/javascript">
            function exit() {
                $('#exit-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        window.location = "/QrcodeManager/index.php/Home/Index/logout";
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        //alert('算求，不退出了');
                    }
                });
            }
        </script>
    </head>
    <body>
        <!--[if lte IE 9]>
        <p class="browsehappy">本网站不支持<strong>过时</strong>的浏览器。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
        <![endif]-->

        <header class="am-topbar admin-header">
            <div class="am-topbar-brand">
                <strong>澄城人武部</strong> <small>二维码体检管理系统</small>
            </div>

            <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

            <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

                <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
                    <li><a href="javascript:;"><span class="am-icon-cog"></span> 设置</a></li>
                    <li><a href="javascript:exit();"><span class="am-icon-power-off"></span> 退出</a></li>
                </ul>
            </div>
        </header>

        <div class="am-cf admin-main">
            <!-- sidebar start -->
            <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
                <div class="am-offcanvas-bar admin-offcanvas-bar">
                    <ul class="am-list admin-sidebar-list">
                        <li><a href="/QrcodeManager/index.php/Home/Index/main"><span class="am-icon-home"></span> 首页</a></li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#renyuan-nav'}"><span class="am-icon-user-secret"></span> 人员管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="renyuan-nav">
                                <li><a href="/QrcodeManager/index.php/Home/People/index"><span class="am-icon-user-plus"></span> 人员资料管理</a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/qrcode"><span class="am-icon-qrcode"></span> 二维码打印表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/look"><span class="am-icon-qrcode"></span> 扫码查看人员 </a></li>
                            </ul>
                        </li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#tijian-nav'}"><span class="am-icon-file"></span> 体检管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="tijian-nav">
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/index"><span class="am-icon-dedent"></span> 体检资料管理</a></li>
                                <li><a href="admin-help.html"><span class="am-icon-table"></span> 体检表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/main"><span class="am-icon-table"></span> 体检结果汇总 </a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="am-panel am-panel-default admin-sidebar-panel">
                        <div class="am-panel-bd">
                            <p><span class="am-icon-bookmark"></span> 现在时间：</p>
                            <script type="text/javascript" src='/QrcodeManager/Public/js/clock.js'></script>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->

            <!-- content start -->
            
<div class="admin-content">

    <div class="am-cf am-padding">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">体检资料管理</strong></div>
    </div>
    <div class="am-g">
        <div class="am-u-sm-12">
            <div class="am-btn-toolbar">
                <div class="am-btn-group">
                    <button type="button" class="am-btn am-btn-default" onclick="addTijian();">添加体检结果</button>
                </div>

                <form class="am-form-inline am-topbar-right" role="search">
                    <div class="am-form-group">
                        <input type="text" class="am-form-field am-input-sm" placeholder="搜索姓名">
                    </div>
                    <button type="submit" class="am-btn am-btn-default am-btn-sm">搜索</button>
                </form>
            </div>
        </div>

        <div class="am-u-sm-12">
            <table class="am-table am-table-bd am-table-striped am-scrollable-horizontal">
                <thead>
                    <tr>
                        <th>身份证号</th>
                        <th>体检结果</th>
                        <th>详细信息</th>
                    </tr>
                </thead>
                <tbody id="tablebody">

                </tbody>
            </table>
        </div>
        <div class="am-u-sm-12">
            <ul class="am-pagination am-pagination-right">
                <!--                            <li class="am-disabled"><a href="#">&laquo;</a></li>-->
                <?php $__FOR_START_4732__=1;$__FOR_END_4732__=$count;for($i=$__FOR_START_4732__;$i < $__FOR_END_4732__;$i+=1){ ?><li id="li<?php echo ($i); ?>"><a href="javascript:refresh(<?php echo ($i); ?>);"><?php echo ($i); ?></a></li><?php } ?>
                <!--                            <li><a href="#">&raquo;</a></li>-->
            </ul>
        </div>
        <div class="am-u-sm-12">
            <div id="doc-qrcode" class="am-text-center"></div>
        </div>
    </div>
</div>


<div class="am-popup" id="addTijian">
    <div class="am-popup-inner">
        <div class="am-popup-hd">
            <h4 class="am-popup-title">添加体检结果</h4>
            <span data-am-modal-close
                  class="am-close">&times;</span>
        </div>
        <div class="am-popup-bd">
            <form class="am-form" id="qrform" onsubmit="isPeople()">
                <div class="am-form-group">
                    <label for="qrcode">二维码</label>
                    <input type="text" id="qrcode" name="qrcode" placeholder="扫描二维码">
                </div>
                <div id="myalert" class="am-alert am-alert-warning" hidden>
                    <button type="button" class="am-close">&times;</button>
                    <p>数据库中没有该人的体验资料，请确保正确扫描二维码！</p>
                </div>
            </form>
            <form class="am-form" id="addform">
                <fieldset id="field" disabled>
                    <input type="hidden" name="cid" id="cid">
                    <p style="text-align: center;">
                        <span style="font-size: 24px;">应征公民体格检查表</span><br/>
                    </p>
                    <p>
                        <span style="font-size: 24px;"></span>
                    </p>
                    <table class="am-table am-table-bordered am-table-centered">
                        <tbody>

                            <!-------------------外科----------------------------->
                            <tr>
                                <td rowspan="7" colspan="1" class="am-text-middle" >外科</td>
                                <td>身高(cm)</td>
                                <td><input type="number" class="am-input-sm" name="shengao"></td>
                                <td>体重(kg)</td>
                                <td><input type="number" class="am-input-sm" name="tizhong"></td>
                            </tr>
                            <tr>
                                <td>病史</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="bingshi-waike"></td>
                            </tr>
                            <tr>
                                <td>头颈部</td>
                                <td><input type="text" class="am-input-sm" name="toujingbu"></td>
                                <td>脊柱</td>
                                <td><input type="text" class="am-input-sm" name="jizhu"></td>
                            </tr>
                            <tr>
                                <td>胸、腹部</td>
                                <td><input type="text" class="am-input-sm" name="xiongfubu"></td>
                                <td>四肢关节</td>
                                <td><input type="text" class="am-input-sm" name="sizhiguanjie"></td>
                            </tr>
                            <tr>
                                <td>泌尿、生殖</td>
                                <td><input type="text" class="am-input-sm" name="miniao"></td>
                                <td>肛门</td>
                                <td><input type="text" class="am-input-sm" name="gangmen"></td>
                            </tr>
                            <tr>
                                <td>皮肤、文身</td>
                                <td><input type="text" class="am-input-sm" name="wenshen"></td>
                                <td>其他</td>
                                <td><input type="text" class="am-input-sm" name="qita-waike"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-waike"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-waike">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <!------------------内科----------------------------->
                            <tr>
                                <td rowspan="6" colspan="1" class="am-text-middle" >内科</td>
                                <td>血压(mmHg)</td>
                                <td><input type="number" class="am-input-sm" name="xueya"></td>
                                <td>口吃</td>
                                <td><input type="text" class="am-input-sm" name="kouchi"></td>
                            </tr>
                            <tr>
                                <td>病史</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="bingshi-neike"></td>
                            </tr>
                            <tr>
                                <td>心率</td>
                                <td colspan="2"><input type="text" class="am-input-sm" name="xinlv"></td>
                                <td>次/分</td>
                            </tr>
                            <tr>
                                <td>肺</td>
                                <td><input type="text" class="am-input-sm" name="fei"></td>
                                <td>腹部</td>
                                <td><input type="text" class="am-input-sm" name="fubu"></td>
                            </tr>
                            <tr>
                                <td>神经</td>
                                <td><input type="text" class="am-input-sm" name="shenjing"></td>
                                <td>其他</td>
                                <td><input type="text" class="am-input-sm" name="qita-neike"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-neike"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-neike">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>
                            <!--                                    -----------------眼科----------------------------->
                            <tr>
                                <td rowspan="5" colspan="1" class="am-text-middle" >眼科</td>
                                <td>右眼</td>
                                <td><input type="number" class="am-input-sm" name="lysl-you" placeholder="裸眼视力"></td>
                                <td><input type="number" class="am-input-sm" name="jzsl-you" placeholder="矫正视力"></td>
                                <td><input type="number" class="am-input-sm" name="jzds-you" placeholder="矫正度数"></td>
                            </tr>
                            <tr>
                                <td>左眼</td>
                                <td><input type="number" class="am-input-sm" name="lysl-zuo" placeholder="裸眼视力"></td>
                                <td><input type="number" class="am-input-sm" name="jzsl-zuo" placeholder="矫正视力"></td>
                                <td><input type="number" class="am-input-sm" name="jzds-zuo" placeholder="矫正度数"></td>
                            </tr>
                            <tr>
                                <td class="am-text-middle">色觉</td>
                                <td colspan="3">
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="sejue" value="正常"> 正常
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="sejue" value="色弱"> 色弱
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="sejue" value="色盲"> 色盲
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="sejue" value="单色识别能力正常"> 单色识别能力正常
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>病史</td>
                                <td><input type="text" class="am-input-sm" name="bingshi-yanke"></td>
                                <td>眼病</td>
                                <td><input type="text" class="am-input-sm" name="yanbing"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-yanke"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-yanke">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>
                            <!--                                    -----------------耳鼻咽喉科----------------------------->
                            <tr>
                                <td rowspan="8" colspan="1" class="am-text-middle" >耳鼻咽喉科</td>
                                <td>听力(右)</td>
                                <td><input type="number" class="am-input-sm" name="tingli-zuo"></td>
                                <td>听力(左)</td>
                                <td><input type="number" class="am-input-sm" name="tingli-you"></td>
                            </tr>
                            <tr>
                                <td class="am-text-middle">嗅觉</td>
                                <td colspan="3">
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="xiujue" value="正常"> 正常
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="xiujue" value="色弱"> 迟钝
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="xiujue" value="色盲"> 丧失
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>病史</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="bingshi-er"></td>
                            </tr>
                            <tr>
                                <td>耳</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="er"></td>
                            </tr>
                            <tr>
                                <td>鼻</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="bi"></td>
                            </tr>
                            <tr>
                                <td>咽喉</td>
                                <td colspan="3"><input type="text" class="am-input-sm" name="yanhou"></td>
                            </tr>
                            <tr>
                                <td>耳气压功能</td>
                                <td><input type="text" class="am-input-sm" name="erqiya"></td>
                                <td>鼓膜情况</td>
                                <td><input type="text" class="am-input-sm" name="gumo"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-er"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-er">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <!-------------------口腔科----------------------------->
                            <tr>
                                <td rowspan="3" colspan="1" class="am-text-middle" >口腔科</td>
                                <td colspan="4">
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="龋齿"> 龋齿
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="牙周炎"> 牙周炎
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="牙颌"> 牙颌、超颌、反颌、深覆颌
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="缺齿"> 缺齿
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="牙列不齐"> 牙列不齐
                                    </label>
                                    <label class="am-checkbox-inline">
                                        <input type="checkbox" name="kouqiang" value="其它"> 其它口腔疾病
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-kouqiang"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-kouqiang">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <!-------------------实验室检查----------------------------->
                            <tr>
                                <td rowspan="7" colspan="1" class="am-text-middle" >实验室检查</td>
                                <td>血常规</td>
                                <td></td>
                                <td>ALT</td>
                                <td><input type="text" class="am-input-sm" name="alt"></td>
                            </tr>
                            <tr>
                                <td>CR</td>
                                <td><input type="text" class="am-input-sm" name="cr"></td>
                                <td>UREA</td>
                                <td><input type="text" class="am-input-sm" name="urea"></td>
                            </tr>
                            <tr>
                                <td>HBsAg</td>
                                <td><input type="text" class="am-input-sm" name="hbsag"></td>
                                <td>HIV抗体</td>
                                <td><input type="text" class="am-input-sm" name="hiv"></td>
                            </tr>
                            <tr>
                                <td>尿常规</td>
                                <td><input type="text" class="am-input-sm" name="niaochanggui"></td>
                                <td>尿沉渣镜检</td>
                                <td><input type="text" class="am-input-sm" name="niaochenjing"></td>
                            </tr>
                            <tr>
                                <td>尿毒品</td>
                                <td><input type="text" class="am-input-sm" name="niaodupin"></td>
                                <td>尿HCG</td>
                                <td><input type="text" class="am-input-sm" name="niaohcg"></td>
                            </tr>
                            <tr>
                                <td>血清HCG</td>
                                <td><input type="text" class="am-input-sm" name="xuehcg"></td>
                                <td>粪便常规</td>
                                <td><input type="text" class="am-input-sm" name="fenbian"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-shiyan"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-shiyan">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <!-------------------士兵职业基本适应性检测----------------------------->
                            <tr>
                                <td class="am-text-middle" >士兵职业基本适应性检测</td>
                                <td>综合结论</td>
                                <td><input type="text" class="am-input-sm" name="zonghejielun-shibing"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-shibing">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td rowspan="2" class="am-text-middle" >胸部X线</td>
                                <td colspan="4"><input type="text" class="am-input-sm" name="xiongbux"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-xiongbux"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-xiongbux">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td rowspan="2" class="am-text-middle" >心电图</td>
                                <td colspan="4"><input type="text" class="am-input-sm" name="xindiantu"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-xindiantu"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-xindiantu">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td rowspan="2" class="am-text-middle" >腹部B超</td>
                                <td colspan="4"><input type="text" class="am-input-sm" name="fubub"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-fubub"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-fubub">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td rowspan="2" class="am-text-middle" >妇科B超</td>
                                <td colspan="4"><input type="text" class="am-input-sm" name="fukeb"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-fukeb"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-fukeb">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <!-------------------妇科----------------------------->
                            <tr>
                                <td rowspan="3" colspan="1" class="am-text-middle" >妇科</td>
                                <td>病史</td>
                                <td><input type="text" class="am-input-sm" name="bingshi-fuke"></td>
                                <td>疾病</td>
                                <td><input type="text" class="am-input-sm" name="jibing-fuke"></td>
                            </tr>
                            <tr>
                                <td>初潮</td>
                                <td><input type="text" class="am-input-sm" name="chuchao"></td>
                                <td>末次月经</td>
                                <td><input type="text" class="am-input-sm" name="mociyuejing"></td>
                            </tr>
                            <tr>
                                <td>医师意见</td>
                                <td><input type="text" class="am-input-sm" name="yishiyijian-fuke"></td>
                                <td>签名</td>
                                <td>
                                    <select name="qianming-fuke">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td rowspan="2" class="am-text-middle" >主检医师意见</td>
                                <td colspan="4"><input type="text" class="am-input-sm" name="yishiyijian-zhujian"></td>
                            </tr>
                            <tr>
                                <td colspan="2">主检签名</td>
                                <td colspan="2">
                                    <select name="qianming-zhujian">
                                        <?php if(is_array($doctors)): $i = 0; $__LIST__ = $doctors;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["id"]); ?>"><?php echo ($vo["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td  class="am-text-middle" >体格结果及结论</td>
                                <td colspan="4">
                                    <label class="am-radio-inline">
                                        <input type="radio"  value="1" name="result" id="result"> 合格
                                    </label>
                                    <label class="am-radio-inline">
                                        <input type="radio" value="0" name="result"> 不合格
                                    </label>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <p><button type="button" class="am-btn am-btn-default" id="submit">提交</button></p>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function addTijian() {
        $('#addTijian').modal();
        $('#qrcode').focus();
    }

    function refresh($page) {
        $.ajax({
            url: "/QrcodeManager/index.php/Home/Tijian/getTable",
            type: "POST",
            cache: false,
            data: "page=" + $page,
            success: function (data) {
                $("#tablebody").html(data);
            },
            error: function () {
                alert("失败");
            }
        });
    }

    function prevnext() {

    }

    function isPeople() {
        event.preventDefault();
        $.ajax({
            url: "/QrcodeManager/index.php/Home/Tijian/isPeople",
            dataType: 'JSON',
            type: "POST",
            cache: false,
            data: $("#qrform").serialize(),
            success: function (data) {
                if (data.s == "ok") {
                    $("#cid").val(data.cid);
                    $("#field").removeAttr("disabled");
                    $("#myalert").alert('close');
                    $("#result").focus();
                }
                else
                {
                    $("#myalert").removeAttr("hidden");
                    $("#myalert").attr("data-am-alert", "data-am-alert");

                    //alert("没人该人的体验资料，请确保正确扫描二维码！");
                }
            },
            error: function () {
                alert("无此人信息，请核对二维码是否正确！");
            }
        });

    }

    $(function () {
        $("#qrcode").keydown(function (event) {
            if (event.which == 13) {
                isPeople();
            }
        });

        refresh(1);

        $("#submit").click(function (event) {
            event.preventDefault();
            $.ajax({
                url: "/QrcodeManager/index.php/Home/Tijian/addTijian",
                dataType: 'JSON',
                type: "POST",
                cache: false,
                data: $("#addform").serialize(),
                success: function (data) {
                    if (data.s == "ok") {
//                                                        $('#addTijian').modal('close');

                        document.getElementById("addform").reset();
                        document.getElementById("qrform").reset();
                        alert("添加成功");
                        $('#qrcode').focus();
                    }
                },
                error: function () {
                    document.getElementById("qrform").reset();
                    document.getElementById("addform").reset();

                    alert("添加失败，信息已存在或服务器错误！");
                    $('#qrcode').focus();

                }
            });
        });
    });

</script>
            <!-- content end -->
        </div>
        <a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
        <footer class="footer" style="text-align: center;">
            <p>© 2015 by 澄城县人民武装部.</p>
        </footer>
        <div class="am-modal am-modal-confirm" tabindex="-1" id="exit-confirm">
            <div class="am-modal-dialog">
                <div class="am-modal-hd">退出</div>
                <div class="am-modal-bd">
                    确定要退出系统吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-confirm>确定</span>
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                </div>
            </div>
        </div>
        
    </body>
</html>