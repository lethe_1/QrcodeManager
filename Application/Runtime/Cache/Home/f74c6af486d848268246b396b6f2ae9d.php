<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>二维码管理系统</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp"/>
        
        <link rel="stylesheet" href="/QrcodeManager/Public/DataTables/media/css/jquery.dataTables.css">
        <link rel="alternate icon" type="image/png" href="/QrcodeManager/Public/i/favicon.png">
        <link rel="stylesheet" href="/QrcodeManager/Public/css/amazeui.min.css"/>
        <link rel="stylesheet" href="/QrcodeManager/Public/css/admin.css">
        
        <!--[if lt IE 9]>
        <script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
        <script src="/QrcodeManager/Public/js/amazeui.ie8polyfill.min.js"></script>
        <![endif]-->

        <!--[if (gte IE 9)|!(IE)]><!-->
        <script src="/QrcodeManager/Public/js/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="/QrcodeManager/Public/js/amazeui.min.js"></script>
        <script src="/QrcodeManager/Public/DataTables/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
            function exit() {
                $('#exit-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        window.location = "/QrcodeManager/index.php/Home/Index/logout";
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        //alert('算求，不退出了');
                    }
                });
            }
        </script>
    </head>
    <body>
        <!--[if lte IE 9]>
        <p class="browsehappy">本网站不支持<strong>过时</strong>的浏览器。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
        <![endif]-->

        <header class="am-topbar admin-header">
            <div class="am-topbar-brand">
                <strong>澄城人武部</strong> <small>二维码体检管理系统</small>
            </div>

            <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

            <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

                <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
                    <li><a href="javascript:;"><span class="am-icon-cog"></span> 设置</a></li>
                    <li><a href="javascript:exit();"><span class="am-icon-power-off"></span> 退出</a></li>
                </ul>
            </div>
        </header>

        <div class="am-cf admin-main">
            <!-- sidebar start -->
            <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
                <div class="am-offcanvas-bar admin-offcanvas-bar">
                    <ul class="am-list admin-sidebar-list">
                        <li><a href="/QrcodeManager/index.php/Home/Index/main"><span class="am-icon-home"></span> 首页</a></li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#renyuan-nav'}"><span class="am-icon-user-secret"></span> 人员管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="renyuan-nav">
                                <li><a href="/QrcodeManager/index.php/Home/People/index"><span class="am-icon-user-plus"></span> 人员资料管理</a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/qrcode"><span class="am-icon-qrcode"></span> 二维码打印表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/look"><span class="am-icon-qrcode"></span> 扫码查看人员 </a></li>
                            </ul>
                        </li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#tijian-nav'}"><span class="am-icon-file"></span> 体检管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="tijian-nav">
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/index"><span class="am-icon-dedent"></span> 体检资料管理</a></li>
                                <li><a href="admin-help.html"><span class="am-icon-table"></span> 体检表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/main"><span class="am-icon-table"></span> 体检结果汇总 </a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="am-panel am-panel-default admin-sidebar-panel">
                        <div class="am-panel-bd">
                            <p><span class="am-icon-bookmark"></span> 现在时间：</p>
                            <script type="text/javascript" src='/QrcodeManager/Public/js/clock.js'></script>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->

            <!-- content start -->
            
<div class="admin-content">
    <div class="am-cf am-padding">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">体检结果汇总</strong></div>
    </div>
    <div class="am-g">
        <div class="am-u-sm-12">
            <div class="am-btn-toolbar">
                <div class="am-btn-group">
                    <button type="button" class="am-btn am-btn-default" onclick="addTijian();">导出Excel</button>
                </div>

                <div class="am-btn-group">

                </div>
            </div>
        </div>
        <div class="am-u-sm-12">
            <hr>
        </div>
        <div class="am-u-sm-12">
            <table id="mainTable" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>姓名</th>
                        <th>性别</th>
                        <th>出生日期</th>
                        <th>文化程度</th>
                        <th>民族</th>
                        <th>婚姻状况</th>
                        <th>职业</th>
                        <th>身份证号</th>
                        <th>体检结果</th>
                    </tr>
                </thead>

                <tfoot>
                    <tr>
                        <th>姓名</th>
                        <th>性别</th>
                        <th>出生日期</th>
                        <th>文化程度</th>
                        <th>民族</th>
                        <th>婚姻状况</th>
                        <th>职业</th>
                        <th>身份证号</th>
                        <th>体检结果</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $("#mainTable").DataTable({
            "language": {
                "lengthMenu": "每页显示 _MENU_ 行",
                "zeroRecords": "Nothing found - sorry",
                "info": "当前是第 _PAGE_ 页，共 _PAGES_ 页",
                "infoEmpty": "没有数据",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate":{
                    "next":"下一页",
                    "previous":"上一页"
                }
            },
            "columns": [
                { "data": "name" },
                { "data": "sex" },
                { "data": "dtime" },
                { "data": "whcd" },
                { "data": "minzu" },
                { "data": "hyzk" },
                { "data": "zhiye" },
                { "data": "cid" },
                { "data": "result" }
            ],
            "ajax": "/QrcodeManager/index.php/Home/Tijian/getTijianResult"
        });
    });
</script>

            <!-- content end -->
        </div>
        <a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
        <footer class="footer" style="text-align: center;">
            <p>© 2015 by 澄城县人民武装部.</p>
        </footer>
        <div class="am-modal am-modal-confirm" tabindex="-1" id="exit-confirm">
            <div class="am-modal-dialog">
                <div class="am-modal-hd">退出</div>
                <div class="am-modal-bd">
                    确定要退出系统吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-confirm>确定</span>
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                </div>
            </div>
        </div>
        
    </body>
</html>