<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="UTF-8">
        <title>二维码管理系统</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp"/>
        
        <link rel="stylesheet" href="/QrcodeManager/Public/DataTables/media/css/jquery.dataTables.css">
        <link rel="alternate icon" type="image/png" href="/QrcodeManager/Public/i/favicon.png">
        <link rel="stylesheet" href="/QrcodeManager/Public/css/amazeui.min.css"/>
        <link rel="stylesheet" href="/QrcodeManager/Public/css/admin.css">
        
        <!--[if lt IE 9]>
        <script src="http://libs.baidu.com/jquery/1.11.1/jquery.min.js"></script>
        <script src="http://cdn.staticfile.org/modernizr/2.8.3/modernizr.js"></script>
        <script src="/QrcodeManager/Public/js/amazeui.ie8polyfill.min.js"></script>
        <![endif]-->

        <!--[if (gte IE 9)|!(IE)]><!-->
        <script src="/QrcodeManager/Public/js/jquery.min.js"></script>
        <!--<![endif]-->
        <script src="/QrcodeManager/Public/js/amazeui.min.js"></script>
        <script src="/QrcodeManager/Public/DataTables/media/js/jquery.dataTables.js"></script>
        <script type="text/javascript">
            function exit() {
                $('#exit-confirm').modal({
                    relatedTarget: this,
                    onConfirm: function (options) {
                        window.location = "/QrcodeManager/index.php/Home/Index/logout";
                    },
                    // closeOnConfirm: false,
                    onCancel: function () {
                        //alert('算求，不退出了');
                    }
                });
            }
        </script>
    </head>
    <body>
        <!--[if lte IE 9]>
        <p class="browsehappy">本网站不支持<strong>过时</strong>的浏览器。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>以获得更好的体验！</p>
        <![endif]-->

        <header class="am-topbar admin-header">
            <div class="am-topbar-brand">
                <strong>澄城人武部</strong> <small>二维码体检管理系统</small>
            </div>

            <button class="am-topbar-btn am-topbar-toggle am-btn am-btn-sm am-btn-success am-show-sm-only" data-am-collapse="{target: '#topbar-collapse'}"><span class="am-sr-only">导航切换</span> <span class="am-icon-bars"></span></button>

            <div class="am-collapse am-topbar-collapse" id="topbar-collapse">

                <ul class="am-nav am-nav-pills am-topbar-nav am-topbar-right admin-header-list">
                    <li><a href="javascript:;"><span class="am-icon-cog"></span> 设置</a></li>
                    <li><a href="javascript:exit();"><span class="am-icon-power-off"></span> 退出</a></li>
                </ul>
            </div>
        </header>

        <div class="am-cf admin-main">
            <!-- sidebar start -->
            <div class="admin-sidebar am-offcanvas" id="admin-offcanvas">
                <div class="am-offcanvas-bar admin-offcanvas-bar">
                    <ul class="am-list admin-sidebar-list">
                        <li><a href="/QrcodeManager/index.php/Home/Index/main"><span class="am-icon-home"></span> 首页</a></li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#renyuan-nav'}"><span class="am-icon-user-secret"></span> 人员管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="renyuan-nav">
                                <li><a href="/QrcodeManager/index.php/Home/People/index"><span class="am-icon-user-plus"></span> 人员资料管理</a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/qrcode"><span class="am-icon-qrcode"></span> 二维码打印表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/People/look"><span class="am-icon-qrcode"></span> 扫码查看人员 </a></li>
                            </ul>
                        </li>
                        <li class="admin-parent">
                            <a class="am-cf" data-am-collapse="{target: '#tijian-nav'}"><span class="am-icon-file"></span> 体检管理 <span class="am-icon-angle-right am-fr am-margin-right"></span></a>
                            <ul class="am-list am-collapse admin-sidebar-sub am-in" id="tijian-nav">
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/index"><span class="am-icon-dedent"></span> 体检资料管理</a></li>
                                <li><a href="admin-help.html"><span class="am-icon-table"></span> 体检表生成 </a></li>
                                <li><a href="/QrcodeManager/index.php/Home/Tijian/main"><span class="am-icon-table"></span> 体检结果汇总 </a></li>
                            </ul>
                        </li>
                    </ul>

                    <div class="am-panel am-panel-default admin-sidebar-panel">
                        <div class="am-panel-bd">
                            <p><span class="am-icon-bookmark"></span> 现在时间：</p>
                            <script type="text/javascript" src='/QrcodeManager/Public/js/clock.js'></script>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar end -->

            <!-- content start -->
            
<div class="admin-content">

    <div class="am-cf am-padding">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">人员资料管理</strong></div>
    </div>
    <div class="am-g">
        <div class="am-u-sm-12">
            <div class="am-btn-toolbar">
                <div class="am-btn-group">
                    <button type="button" class="am-btn am-btn-default" onclick="addPeople();">添加人员</button>
                </div>

                <form class="am-form-inline am-topbar-right" role="search">
                    <div class="am-form-group">
                        <input type="text" class="am-form-field am-input-sm" placeholder="搜索姓名">
                    </div>
                    <button type="submit" class="am-btn am-btn-default am-btn-sm">搜索</button>
                </form>
            </div>
        </div>

        <div class="am-u-sm-12">
            <table class="am-table am-table-bd am-table-striped am-scrollable-horizontal">
                <thead>
                    <tr>
                        <th>身份证号</th>
                        <th>姓名</th>
                        <th>性别</th>
                        <th>出生日期</th>
                        <th>文化程度</th>
                        <th>民族</th>
                        <th>婚姻状况</th>
                        <th>职业</th>
                        <th>毕业学校或工作单位</th>
                        <th>现住址</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="tablebody">

                </tbody>
            </table>
        </div>
        <div class="am-u-sm-12">
            <ul class="am-pagination am-pagination-right">
                <!--                            <li class="am-disabled"><a href="#">&laquo;</a></li>-->
                <?php $__FOR_START_13833__=1;$__FOR_END_13833__=$count;for($i=$__FOR_START_13833__;$i < $__FOR_END_13833__;$i+=1){ ?><li id="li<?php echo ($i); ?>"><a href="javascript:refresh(<?php echo ($i); ?>);"><?php echo ($i); ?></a></li><?php } ?>
                <!--                            <li><a href="#">&raquo;</a></li>-->
            </ul>
        </div>
        <div class="am-u-sm-12">
            <div id="doc-qrcode" class="am-text-center"></div>
        </div>
    </div>
</div>


<div class="am-popup" id="addPeople">
    <div class="am-popup-inner">
        <div class="am-popup-hd">
            <h4 class="am-popup-title">添加人员</h4>
            <span data-am-modal-close
                  class="am-close">&times;</span>
        </div>
        <div class="am-popup-bd">
            <form class="am-form" id="addform">
                <fieldset>
                    <div class="am-form-group">
                        <label for="cid">身份证号</label>
                        <input type="text" id="cid" name="cid" placeholder="输入正确的身份证号码" required>
                    </div>
                    <div class="am-form-group">
                        <label for="name">姓名</label>
                        <input type="text" id="name" name="name" placeholder="输入姓名">
                    </div>
                    <div class="am-form-group">
                        <label for="sex">性别</label>
                        <div>
                            <label class="am-radio-inline">
                                <input type="radio" value="1" name="sex" id="sex1"> 男
                            </label>
                            <label class="am-radio-inline">
                                <input type="radio" value="0" name="sex" id="sex0"> 女
                            </label></div>
                    </div>
                    <div class="am-form-group">
                        <label for="chushengriqi">出生日期</label>
                        <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
                            <input id="chushengriqi" name="chushengriqi" type="text" class="am-form-field" placeholder="出生日期" readonly>
                            <span class="am-input-group-btn am-datepicker-add-on">
                                <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
                            </span>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="whcd">文化程度</label>
                        <select id="whcd" name="whcd">
                            <?php if(is_array($whcd)): $i = 0; $__LIST__ = $whcd;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["wid"]); ?>"><?php echo ($vo["whcd"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        <span class="am-form-caret"></span>
                    </div>
                    <div class="am-form-group">
                        <label for="minzu">民族</label>
                        <input type="text" id="minzu" name="minzu" placeholder="民族">
                    </div>

                    <div class="am-form-group">
                        <label for="hyzk">婚姻状况</label>
                        <select id="hyzk" name="hyzk">
                            <option value="未婚">未婚</option>
                            <option value="已婚">已婚</option>
                            <option value="丧偶">丧偶</option>
                        </select>
                        <span class="am-form-caret"></span>
                    </div>
                    <div class="am-form-group">
                        <label for="zhiye">职业</label>
                        <input type="text" id="zhiye" name="zhiye" placeholder="填写从事职业">
                    </div>
                    <div class="am-form-group">
                        <label for="gzdw">毕业学校或工作单位</label>
                        <input type="text" id="gzdw" name="gzdw" placeholder="毕业学校或工作单位">
                    </div>
                    <div class="am-form-group">
                        <label for="address">现住址</label>
                        <input type="text" id="address" name="address" placeholder="家庭详细住址">
                    </div>

                    <p><button type="button" class="am-btn am-btn-default" id="submit">提交</button></p>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<div class="am-popup" id="editPeople">
    <div class="am-popup-inner">
        <div class="am-popup-hd">
            <h4 class="am-popup-title">编辑人员</h4>
            <span data-am-modal-close
                  class="am-close">&times;</span>
        </div>
        <div class="am-popup-bd">
            <form class="am-form" id="editform">
                <fieldset>
                    <div class="am-form-group">
                        <label for="e_cid">身份证号</label>
                        <input type="text" id="e_cid" name="cid" placeholder="输入正确的身份证号码" readonly>
                    </div>
                    <div class="am-form-group">
                        <label for="e_name">姓名</label>
                        <input type="text" id="e_name" name="name" placeholder="输入姓名">
                    </div>
                    <div class="am-form-group">
                        <label for="sex">性别</label>
                        <div>
                            <label class="am-radio-inline">
                                <input type="radio" value="1" name="sex" id="e_sex1"> 男
                            </label>
                            <label class="am-radio-inline">
                                <input type="radio" value="0" name="sex" id="e_sex0"> 女
                            </label></div>
                    </div>
                    <div class="am-form-group">
                        <label for="e_chushengriqi">出生日期</label>
                        <div class="am-input-group am-datepicker-date" data-am-datepicker="{format: 'yyyy-mm-dd'}">
                            <input id="e_chushengriqi" name="chushengriqi" type="text" class="am-form-field" placeholder="出生日期" readonly>
                            <span class="am-input-group-btn am-datepicker-add-on">
                                <button class="am-btn am-btn-default" type="button"><span class="am-icon-calendar"></span> </button>
                            </span>
                        </div>
                    </div>
                    <div class="am-form-group">
                        <label for="e_whcd">文化程度</label>
                        <select id="e_whcd" name="whcd">
                            <?php if(is_array($whcd)): $i = 0; $__LIST__ = $whcd;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo["wid"]); ?>"><?php echo ($vo["whcd"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                        <span class="am-form-caret"></span>
                    </div>
                    <div class="am-form-group">
                        <label for="e_minzu">民族</label>
                        <input type="text" id="e_minzu" name="minzu" placeholder="民族">
                    </div>

                    <div class="am-form-group">
                        <label for="e_hyzk">婚姻状况</label>
                        <select id="e_hyzk" name="hyzk">
                            <option value="未婚">未婚</option>
                            <option value="已婚">已婚</option>
                            <option value="丧偶">丧偶</option>
                        </select>
                        <span class="am-form-caret"></span>
                    </div>
                    <div class="am-form-group">
                        <label for="e_zhiye">职业</label>
                        <input type="text" id="e_zhiye" name="zhiye" placeholder="填写从事职业">
                    </div>
                    <div class="am-form-group">
                        <label for="e_gzdw">毕业学校或工作单位</label>
                        <input type="text" id="e_gzdw" name="gzdw" placeholder="毕业学校或工作单位">
                    </div>
                    <div class="am-form-group">
                        <label for="e_address">现住址</label>
                        <input type="text" id="e_address" name="address" placeholder="家庭详细住址">
                    </div>

                    <p><button type="button" class="am-btn am-btn-default" id="e_submit" onclick="editSubmit()">提交</button></p>
                </fieldset>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    function addPeople() {
        $('#addPeople').modal();
    }

    function exit() {
        $('#exit-confirm').modal({
            relatedTarget: this,
            onConfirm: function (options) {
                window.location = "/QrcodeManager/index.php/Home/Index/logout";
            },
            // closeOnConfirm: false,
            onCancel: function () {
                //alert('算求，不退出了');
            }
        });
    }

    function edit(id) {
        $.ajax({
            url: "/QrcodeManager/index.php/Home/People/getPeople",
            type: "POST",
            dataType: 'JSON',
            cache: false,
            data: "id=" + id,
            success: function (data) {
                $('#editPeople').modal();
                $("#e_cid").val(data.cid);
                $("#e_name").val(data.name);
                $("#e_chushengriqi").val(data.dtime);
                $("#e_minzu").val(data.minzu);
                $("#e_zhiye").val(data.zhiye);
                $("#e_gzdw").val(data.gzdw);
                $("#e_address").val(data.address);
                $("#e_hyzk").val(data.hyzk);
                $("#e_whcd").val(data.whcd);
                if (data.sex == "1") {
                    $("#e_sex1").attr("checked", "checked");
                } else {
                    $("#e_sex0").attr("checked", "checked");
                }
            }
        });
    }

    function editSubmit() {
        event.preventDefault();
        $.ajax({
            url: "/QrcodeManager/index.php/Home/People/editPeople",
            dataType: 'JSON',
            type: "POST",
            cache: false,
            data: $("#editform").serialize(),
            success: function (data) {
                if (data.s == "ok") {
                    $('#editPeople').modal('close');
                    document.getElementById("editform").reset();
                    refresh(1);
                    alert("修改成功");
                } else
                {
                    alert("添加失败：" + data.s);
                }
            },
            error: function () {
                alert("失败");
            }
        });
    }

    function del(id) {
        alert("del" + id);
    }

    function qrcode($text) {
        $qr = $('#doc-qrcode');
        $qr.empty().qrcode($text);
    }

    function refresh($page) {
        $.ajax({
            url: "/QrcodeManager/index.php/Home/People/getTable",
            type: "POST",
            cache: false,
            data: "page=" + $page,
            success: function (data) {
                $("#tablebody").html(data);
            },
            error: function () {
                alert("失败");
            }
        });
    }

    function prevnext() {

    }

    $(function () {
        refresh(1);
        $("#submit").click(function (event) {
            event.preventDefault();
            $.ajax({
                url: "/QrcodeManager/index.php/Home/People/addPeople",
                dataType: 'JSON',
                type: "POST",
                cache: false,
                data: $("#addform").serialize(),
                success: function (data) {
                    if (data.s == "ok") {
                        $('#addPeople').modal('close');
                        document.getElementById("addform").reset();
                        refresh(1);
                        alert("添加成功");
                    } else
                    {
                        alert("添加失败：" + data.s);
                    }
                },
                error: function () {
                    alert("失败");
                }
            });
        });
    });

</script>

            <!-- content end -->
        </div>
        <a href="#" class="am-icon-btn am-icon-th-list am-show-sm-only admin-menu" data-am-offcanvas="{target: '#admin-offcanvas'}"></a>
        <footer class="footer" style="text-align: center;">
            <p>© 2015 by 澄城县人民武装部.</p>
        </footer>
        <div class="am-modal am-modal-confirm" tabindex="-1" id="exit-confirm">
            <div class="am-modal-dialog">
                <div class="am-modal-hd">退出</div>
                <div class="am-modal-bd">
                    确定要退出系统吗？
                </div>
                <div class="am-modal-footer">
                    <span class="am-modal-btn" data-am-modal-confirm>确定</span>
                    <span class="am-modal-btn" data-am-modal-cancel>取消</span>
                </div>
            </div>
        </div>
        
    </body>
</html>