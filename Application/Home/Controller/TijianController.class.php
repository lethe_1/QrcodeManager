<?php
namespace Home\Controller;
use Think\Controller;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TijianController
 *
 * @author lethe
 */
class TijianController extends Controller {
    public function index(){
        A('Common');
        
        $doctor=M('doctor');
        $list=$doctor->select();
        $this->assign("doctors",$list);
        
        $tijian=M('tijian');
        $this->assign('count',count($tijian->select())/10+1);
        $this->display();
    }
    
    public function isPeople(){
        $people=M('people');
        $condition['Cid']= trim(decrypt($_POST['qrcode']));
        $p=$people->where($condition)->find();
        if($p){
            $data['cid']=$p['cid'];
            $data['s']='ok';
            exit(json_encode($data));
        }else
        {
            $data['reason']='no person';
            $data['s']='false';
            exit(json_encode($data));
        }
    }
    
    public function testED(){
        $method=$_POST['m'];
        $s=$_POST['s'];
        if($method=='E'){
            R('Public/errjson',array(encrypt($s)));
        }else
        {
            R('Public/errjson',array(decrypt($s)));
        }
    }
    
    public function testDatabase(){
        $method=$_POST['m'];
        $s=$_POST['s2'];
        $people=M('people');
        $con['Cid']='';
        if($method=='E'){
            $con['Cid']=  trim(decrypt($s));
        }else
        {
            $con['Cid']=$s;
        }
        $p=$people->where($con)->find();
        R('Public/errjson',array(json_encode($p)));
    }
    
    public function addTijian(){
        if ($_POST['cid']) {
            $tijian=M('tijian');
            $data['cid']=$_POST['cid'];
            $data['result']=$_POST['result'];
              
            if ($tijian->data($data)->add()) {
                R('Public/errjson', array('ok'));
            }
        }  else {
            R('Public/errjson', array('无法识别身份信息！'));
        }
    }
    
    public function getTijianResult(){
        $tijian=M('tijian');
        $list=$tijian->join(' LEFT JOIN tp_people ON tp_tijian.cid=tp_people.Cid ')->join(' LEFT JOIN tp_whcd ON tp_people.whcd=tp_whcd.wid ')->select();
        $r['data']=$list;
        echo json_encode($r);
    }
    
    public function getPrintResult(){
        $tijian=M('tijian');
        $con =  json_decode($_POST['aoData']);
        $list=$tijian->join(' LEFT JOIN tp_people ON tp_tijian.cid=tp_people.Cid ')->join(' LEFT JOIN tp_whcd ON tp_people.whcd=tp_whcd.wid ')->where($tijian)->select();
        $r['data']=$list;
        echo json_encode($r);
    }
}
