<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
        //检测是否登录
        if(isset($_SESSION['ThinkUser'])){
            $this->redirect('Index/main');
        }else {
            $this->display();
        }
    }
    
    public function main(){
        A('Common');
        $this->session=$_SESSION['ThinkUser'];
        $this->display();
    }

    public function login(){
        if(IS_AJAX) {
            $login = array();
            $username = I('post.username', '', 'htmlspecialchars');
            $password = I('post.password', '');
            if (!preg_match('/^[\x{4e00}-\x{9fa5}a-zA-Z0-9_-]{2,16}$/u', $username)) {
                R('Public/errjson', array('请输入合法的用户名'));
            }
            if (strlen($password) < 6 || strlen($password) > 18) {
                R('Public/errjson', array('请输入6位数以上的密码'));
            }
            $user = M('user');
            $condition['Username']=$username;
            if ($re = $user->where($condition)->count()) {
                $condition['Password']=sha1(md5($password));
                if($result=$user->where($condition)->getField('ID,Username,Password,Roleid,Status,Competence,Loginarea,Logincount')){
                    //将二维数组转为一维数组
                    foreach($result as $key => $val) {
                        $arr = $val;
                    }
                    $_SESSION['ThinkUser'] = $arr;
                    R('Public/errjson',array('ok'));
                }else {
                    R('Public/errjson',array('登录密码错误！'));
                }
            }else {
                R('Public/errjson',array('用户名不存在'));
            }
        }else{
            R('Public/errjson',array('非法请求'));
        }
    }
    
    public function logout(){
        session('[destroy]');
        $this->redirect("Index/index");
    }

    public function qrcode($text = 'NULL', $level = 3, $size = 4)
    {
        Vendor('phpqrcode.phpqrcode');
        $errorCorrectionLevel = intval($level);//容错级别
        $matrixPointSize = intval($size);//生成图片大小
        //生成二维码图片
        //echo $_SERVER['REQUEST_URI'];
        $object = new \QRcode();
        $object->png($text, false, $errorCorrectionLevel, $matrixPointSize, 2);
    }
}