<?php
namespace Home\Controller;
use Think\Controller;

/**
 * Description of PeopleController
 *
 * @author Lethe
 */
class PeopleController extends Controller {
    public function index(){
        A('Common');
        $whcd=M('whcd');
        $this->assign('whcd',$whcd->select());
        $this->display();
    }
    
    public function qrcode(){
        A('Common');
        $this->display();
    }
    
    public function getQrcodeResult(){
        $people=M('people');
        $list=$people->select();
        for($i=0;$i<count($list);++$i)
        {
            $list[$i]['qrcode']=encrypt($list[$i]['cid']);
            $list[$i]['demo']=$list[$i]['name'].'('.$list[$i]['cid'].')';
        }
        $r['data']=$list;
        echo json_encode($r);
    }
    
    public function exportexcel($filename = 'qrcodeReport') {
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
        header("Content-type:application/vnd.ms-excel");
        header("Content-Disposition:attachment;filename=" . $filename . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        //导出xls 开始
        $tag0=  iconv("UTF-8", "GB2312", '二维码编号');
        $tag1=  iconv("UTF-8", "GB2312", '文本标识');
        echo "$tag0\t$tag1\n";

        $people=M('people');
        $list=$people->field('cid,name')->select();
        
        foreach ($list as $key=>$val){
            $qrcode=iconv("UTF-8","GB2312",encrypt(trim($val['cid'])));
            $qrcode=$qrcode?$qrcode:'-';
//            $demo=  iconv("UTF-8", "GB2312", trim($val['name']).'('.trim($val['cid']).')');
            $demo=  iconv("UTF-8", "GB2312", trim($val['name']));
            $demo=$demo?$demo:'-';
            echo "$qrcode\t$demo\n";
        }
    }
    
    public function  addPeople(){
        if ($_POST['cid'] && $_POST['name']) {
            $people = M('people');
            $data['Name'] = $_POST['name'];
            $data['Cid'] = $_POST['cid'];
            $data['Address'] = $_POST['address'];
            $data['Sex'] = $_POST['sex'];
            $data['Dtime'] = $_POST['chushengriqi'];
            $data['Minzu'] = $_POST['minzu'];
            $data['gzdw'] = $_POST['gzdw'];
            $data['zhiye'] = $_POST['zhiye'];
            $data['whcd'] = $_POST['whcd'];
            $data['hyzk'] = $_POST['hyzk'];
            
            if ($people->data($data)->add()) {
                R('Public/errjson', array('ok'));
            }
        }  else {
            R('Public/errjson', array('身份证号码或用户名为空'));
        }
//        dump($people);
    }
    
    public function  editPeople(){

        if ($_POST['cid'] && $_POST['name']) {
            $con['Cid']=$_POST['cid'];
            $people = M('people');
            $data['Name'] = $_POST['name'];
            $data['Address'] = $_POST['address'];
            $data['Sex'] = $_POST['sex'];
            $data['Dtime'] = $_POST['chushengriqi'];
            $data['Minzu'] = $_POST['minzu'];
            $data['gzdw'] = $_POST['gzdw'];
            $data['zhiye'] = $_POST['zhiye'];
            $data['whcd'] = $_POST['whcd'];
            $data['hyzk'] = $_POST['hyzk'];
            
            if ($people->where($con)->save($data)==FALSE) {
                R('Public/errjson', array('false'));
            }  else {
                R('Public/errjson', array('ok'));
            }
        }  else {
            R('Public/errjson', array('身份证号码或用户名为空'));
        }
    }
    
    public function findPeople(){
        $people=M('people');
        $condition['Cid']= trim(decrypt($_POST['qrcode']));
        $p=$people->where($condition)->find();
        $whcd=M('whcd');
        $whcdList=$whcd->select();
        if($p){
            echo '<table class="am-table am-table-bordered">';
            echo '<tr>';
            echo '<td>姓名：</td>';
            echo '<td>',$p['name'],'</td>';
            echo '<td>身份证号：</td>';
            echo '<td>',$p['cid'],'</td>';
            echo '</tr>';
            echo '<tr>';
            $sex=($p['sex'])?'男':'女';
            echo '<td>性别：</td>';
            echo '<td>',$sex,'</td>';
            echo '<td>出生日期：</td>';
            echo '<td>',$p['dtime'],'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>文化程度：</td>';
            echo '<td>',$whcdList[$p['whcd']-1]['wwhcd'],'</td>';
            echo '<td>民族：</td>';
            echo '<td>',$p['minzu'],'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>职业：</td>';
            echo '<td>',$p['zhiye'],'</td>';
            echo '<td>毕业学校或工作单位：</td>';
            echo '<td>',$p['gzdw'],'</td>';
            echo '</tr>';
            echo '<tr>';
            echo '<td>婚姻状况：</td>';
            echo '<td>',$p['hyzk'],'</td>';
            echo '<td>现住址：</td>';
            echo '<td>',$p['address'],'</td>';
            echo '</tr>';
            echo '</table>';
        }else
        {
            echo '<h1>查无此人</h1>';
        }
    }
    
    public function getPeopleResult(){
        $people=M('people');
        $list=$people->join(' LEFT JOIN tp_whcd ON tp_people.whcd=tp_whcd.wid ')->select();
        for($i=0;$i<count($list);++$i)
        {
            $list[$i]['qrcode']=encrypt($list[$i]['cid']);
        }
        $r['data']=$list;
        echo json_encode($r);
    }
}

?>
